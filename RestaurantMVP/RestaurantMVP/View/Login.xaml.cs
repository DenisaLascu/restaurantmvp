﻿using RestaurantMVP.Model;
using RestaurantMVP.View;
using RestaurantMVP.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RestaurantMVP
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public Login()
        {
            InitializeComponent();
            UsersManager.Update();
        }

        private void btn_newAccount_Click(object sender, RoutedEventArgs e)
        {
            Window newUserWindow = new NewAccount();
            newUserWindow.Show();
            Close();
        }

        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            if (txt_username.Text != "Username" && txt_username.Text != "" && txt_password.Password != "")
            {
               
                if(UsersManager.VerifyTypeOfUser(txt_username.Text)==1)
                {
                    if (UsersManager.VerifyPassword(UsersManager.FindUser(txt_username.Text), txt_password.Password) == true)
                    {
                        Window window = new Management();
                        window.Show();
                        Close();
                    }
                    else
                        MessageBox.Show("Parola incorecta.");
                }
                else
                    if(UsersManager.VerifyTypeOfUser(txt_username.Text) == 2)
                {
                    if (UsersManager.VerifyPassword(UsersManager.FindUser(txt_username.Text), txt_password.Password) == true)
                    {
                        Window meniu = new MainWindow();
                        meniu.Show();
                        Close();
                    }
                    else
                        MessageBox.Show("Parola incorecta.");
                }
                else
                    MessageBox.Show("Utilizatorul nu exista. Creati un cont nou.");

            }
            else
                if (txt_username.Text == "Username" || txt_username.Text == "")
                MessageBox.Show("Introduceti datele contului existent");
            else 
                if(txt_password.Password=="")
                MessageBox.Show("Introduceti parola");
        }

        private void txt_username_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_username.Text = "";
        }

        private void txt_password_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_password.Password = "";
        }
    }
}
