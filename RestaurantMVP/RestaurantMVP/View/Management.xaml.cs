﻿using RestaurantMVP.Model;
using RestaurantMVP.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RestaurantMVP.View
{
    /// <summary>
    /// Interaction logic for Management.xaml
    /// </summary>
    public partial class Management : Window
    {
        RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public Management()
        {
            InitializeComponent();
            if (dc.DatabaseExists())
            {
                dataGridPreparat.ItemsSource = dc.Preparats;
                dataGridMeniu.ItemsSource = dc.Menius;
                dataGridMeniuPreparat.ItemsSource = dc.MeniuPreparats;
            }

        }

        private void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            dc.SubmitChanges();
        }

        private void btn_orders_Click(object sender, RoutedEventArgs e)
        {
            Window window = new Orders();
            window.Show();
            Close();
        }
    }
}
