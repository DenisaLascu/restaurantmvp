﻿using RestaurantMVP.Model;
using RestaurantMVP.View_Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RestaurantMVP
{
    /// <summary>
    /// Interaction logic for NewAccount.xaml
    /// </summary>
    public partial class NewAccount : Window
    {
        RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public NewAccount()
        {
            InitializeComponent();
        }

        private void btn_client_Click(object sender, RoutedEventArgs e)
        {
            if (txt_adresa.Text == "" || txt_email.Text == "" || txt_nume.Text == "" || txt_parola.Text == "" || txt_prenume.Text == "" || txt_telefon.Text == "")
                MessageBox.Show("Introduceti toate datele");
            else
            {
                var Connection =System.Configuration.ConfigurationManager.ConnectionStrings["RestaurantMVP.Properties.Settings.RestaurantConnectionString"].ConnectionString;
                var sql = "INSERT INTO Utilizator ([adresa],[email],[nume], [parola], [prenume], [telefon], [tip], [id_Utilizator] ) VALUES (@adresa,@email,@nume,@parola,@prenume,@telefon,@tip,@id_Utilizator) ";
                try
                {
                    using (var connection = new SqlConnection(Connection))
                    {
                        using (var command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@adresa", txt_adresa.Text);
                            command.Parameters.AddWithValue("@email", txt_email.Text);
                            command.Parameters.AddWithValue("@nume", txt_nume.Text);
                            command.Parameters.AddWithValue("@parola", txt_parola.Text);
                            command.Parameters.AddWithValue("@prenume", txt_prenume.Text);
                            command.Parameters.AddWithValue("@telefon", txt_telefon.Text);
                            command.Parameters.AddWithValue("@tip", "client");
                            command.Parameters.AddWithValue("@id_Utilizator", UsersManager.GetNumberOfUsers() + 1);
                            connection.Open();
                            command.ExecuteNonQuery();


                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Failed to update. Error message: {ex.Message}");
                }
                dc.SubmitChanges();
                UsersManager.PopulateListOfClienti();
                Window window = new Login();
                window.Show();
                Close();
            }
        }

        private void btn_return_Click(object sender, RoutedEventArgs e)
        {
            Window window = new Login();
            window.Show();
            Close();
        }

        private void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn_employee_Click(object sender, RoutedEventArgs e)
        {
            if (txt_adresa.Text == "" || txt_email.Text == "" || txt_nume.Text == "" || txt_parola.Text == "" || txt_prenume.Text == "" || txt_telefon.Text == "")
                MessageBox.Show("Introduceti toate datele");
            else
            {
                var Connection = System.Configuration.ConfigurationManager.ConnectionStrings["RestaurantMVP.Properties.Settings.RestaurantConnectionString"].ConnectionString;
                var sql = "INSERT INTO Utilizator ([adresa],[email],[nume], [parola], [prenume], [telefon], [tip], [id_Utilizator] ) VALUES (@adresa,@email,@nume,@parola,@prenume,@telefon,@tip,@id_Utilizator) ";
                try
                {
                    using (var connection = new SqlConnection(Connection))
                    {
                        using (var command = new SqlCommand(sql, connection))
                        {
                            command.Parameters.AddWithValue("@adresa", txt_adresa.Text);
                            command.Parameters.AddWithValue("@email", txt_email.Text);
                            command.Parameters.AddWithValue("@nume", txt_nume.Text);
                            command.Parameters.AddWithValue("@parola", txt_parola.Text);
                            command.Parameters.AddWithValue("@prenume", txt_prenume.Text);
                            command.Parameters.AddWithValue("@telefon", txt_telefon.Text);
                            command.Parameters.AddWithValue("@tip", "angajat");
                            command.Parameters.AddWithValue("@id_Utilizator", UsersManager.GetNumberOfUsers() + 1);
                            connection.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Failed to update. Error message: {ex.Message}");
                }
                UsersManager.PopulateListOfAngajati();
                dc.SubmitChanges();
                Window window = new Login();
                window.Show();
                Close();
            }
        }

        private void txt_nume_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_nume.Text = "";
        }

        private void txt_prenume_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_prenume.Text = "";
        }

        private void txt_email_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_email.Text = "";
        }

        private void txt_telefon_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_telefon.Text = "";
        }

        private void txt_adresa_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_adresa.Text = "";
        }

        private void txt_Parola_GotFocus(object sender, RoutedEventArgs e)
        {
            txt_parola.Text = "";
        }
    }
}
