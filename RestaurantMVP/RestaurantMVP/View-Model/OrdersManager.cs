﻿using RestaurantMVP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantMVP.View_Model
{
    class OrdersManager
    {
        public static RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public static List<Comanda> listaComenzi = new List<Comanda>();
        public static int GetNumberOfOrders()
        {
            return listaComenzi.Count;
        }

        public static void PopulateListOfOrders()
        {
            IEnumerable<Comanda> orders = from o in dc.Comandas select o;
            foreach (Comanda order in orders)
                listaComenzi.Add(order);
        }

        public static List<Preparat> PreparateByCategory(int categorie)
        {
            List<Preparat> listByCategory = new List<Preparat>();
            IEnumerable<Preparat> preparate = from p in dc.Preparats where p.Categorie.id_Categorie == categorie select p;
            foreach (Preparat preparat in preparate)
                listByCategory.Add(preparat);
            return listByCategory;
        }

        public static List<Meniu> MeniuriByCategory(int categorie)
        {
            List<Meniu> listByCategory = new List<Meniu>();
            IEnumerable<Meniu> meniuri = from m in dc.Menius where m.Categorie.id_Categorie == categorie select m;
            foreach (Meniu meniu in meniuri)
                listByCategory.Add(meniu);
            return listByCategory;
        }
        public static float GetCost()
        {

            return 0;
        }
    }
}
