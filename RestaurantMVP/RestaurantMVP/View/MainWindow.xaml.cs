﻿using RestaurantMVP.Model;
using RestaurantMVP.View;
using RestaurantMVP.View_Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestaurantMVP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public MainWindow()
        {
            InitializeComponent();
            if (dc.DatabaseExists())
            {
                dataGridPreparate.ItemsSource = dc.Preparats;
                dataGridMeniuri.ItemsSource = dc.Menius;
                
            }
            
        }

        private void btn_close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn_continue_Click(object sender, RoutedEventArgs e)
        {
            Window cart = new Cart();
            cart.Show();
            Close();
        }


        private void dataGridPreparate_GotFocus(object sender, RoutedEventArgs e)
        {
            var Connection = System.Configuration.ConfigurationManager.ConnectionStrings["RestaurantMVP.Properties.Settings.RestaurantConnectionString"].ConnectionString;
            var sql = "INSERT INTO Comanda ([id_Comanda],[stare],[data], [cost], [transport], [id_Utilizator] ) VALUES (@id_Comanda,@stare,@data,@cost,@transport,@id_Utilizator) ";
            var sql2 = "INSERT INTO ComandaPreparat ([id],[id_Comanda],[id_Preparat],[portii] ) VALUES (@id,@id_Comanda,@id_Preparat,@portii)";
            try
            {
                using (var connection = new SqlConnection(Connection))
                {
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_Comanda", OrdersManager.GetNumberOfOrders()+1);
                        command.Parameters.AddWithValue("@stare", "acceptata");
                        command.Parameters.AddWithValue("@data", DateTime.Now);
                        command.Parameters.AddWithValue("@cost", OrdersManager.GetCost());
                        command.Parameters.AddWithValue("@transport", 3);
                        command.Parameters.AddWithValue("@id_Utilizator", UsersManager.currentUser.id_Utilizator);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }

                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to update. Error message: {ex.Message}");
            }

            try
            {
                using(var connection=new SqlConnection(Connection))
                {
                    using (var command = new SqlCommand(sql2, connection))
                    {
                        command.Parameters.AddWithValue("@id", OrdersManager.GetNumberOfOrders() + 1);
                        command.Parameters.AddWithValue("@id_Comanda", OrdersManager.GetNumberOfOrders() + 1);
                        command.Parameters.AddWithValue("@id_Preparat", dataGridPreparate.SelectedIndex);
                        command.Parameters.AddWithValue("@portii", 1);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to update. Error message: {ex.Message}");
            }

        }



        private void listaCategorii_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<Preparat> listaPreparate = new List<Preparat>();
            listaPreparate = OrdersManager.PreparateByCategory(listaCategorii.SelectedIndex+1);
            dataGridPreparate.ItemsSource = listaPreparate;

            List<Meniu> listaMeniuri = new List<Meniu>();
            listaMeniuri = OrdersManager.MeniuriByCategory(listaCategorii.SelectedIndex + 1);
            dataGridMeniuri.ItemsSource = listaMeniuri;
        }

        private void dataGridMeniuri_GotFocus(object sender, RoutedEventArgs e)
        {
            var Connection = System.Configuration.ConfigurationManager.ConnectionStrings["RestaurantMVP.Properties.Settings.RestaurantConnectionString"].ConnectionString;
            var sql = "INSERT INTO Comanda ([id_Comanda],[stare],[data], [cost], [transport], [id_Utilizator] ) VALUES (@id_Comanda,@stare,@data,@cost,@transport,@id_Utilizator) ";
            var sql2 = "INSERT INTO ComandaMeniu ([id],[id_Comanda],[id_Meniu],[portii] ) VALUES (@id,@id_Comanda,@id_Meniu,@portii)";
            try
            {
                using (var connection = new SqlConnection(Connection))
                {
                    using (var command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@id_Comanda", OrdersManager.GetNumberOfOrders() + 1);
                        command.Parameters.AddWithValue("@stare", "acceptata");
                        command.Parameters.AddWithValue("@data", DateTime.Now);
                        command.Parameters.AddWithValue("@cost", OrdersManager.GetCost());
                        command.Parameters.AddWithValue("@transport", 3);
                        command.Parameters.AddWithValue("@id_Utilizator", UsersManager.currentUser.id_Utilizator);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to update. Error message: {ex.Message}");
            }

            try
            {
                using (var connection = new SqlConnection(Connection))
                {
                    using (var command = new SqlCommand(sql2, connection))
                    {
                        command.Parameters.AddWithValue("@id", OrdersManager.GetNumberOfOrders() + 1);
                        command.Parameters.AddWithValue("@id_Comanda", OrdersManager.GetNumberOfOrders() + 1);
                        command.Parameters.AddWithValue("@id_Meniu", dataGridPreparate.SelectedIndex);
                        command.Parameters.AddWithValue("@portii", 1);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to update. Error message: {ex.Message}");
            }
        }
    }
}
