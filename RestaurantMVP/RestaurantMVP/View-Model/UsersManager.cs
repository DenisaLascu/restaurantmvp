﻿using RestaurantMVP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantMVP.View_Model
{
    class UsersManager
    {
        public static RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public static List<Utilizator> listaClienti=new List<Utilizator>();
        public static List<Utilizator> listaAngajati = new List<Utilizator>();
        public static Utilizator currentUser=new Utilizator();

        public static void PopulateListOfClienti()
        {
            IEnumerable<Utilizator> clienti = from u in dc.Utilizators where u.tip == "client" select u;
            foreach (Utilizator user in clienti)
                listaClienti.Add(user);
        }

        public static void PopulateListOfAngajati()
        {
            IEnumerable<Utilizator> angajati = from u in dc.Utilizators where u.tip == "angajat" select u;
            foreach (Utilizator user in angajati)
                listaAngajati.Add(user);
        }

        public static void Update()
        {
            PopulateListOfClienti();
            PopulateListOfAngajati();
        }

        public static List<Utilizator> GetClienti()
        {
            return listaClienti;
        }

        public static List<Utilizator> GetAngajati()
        {
            return listaAngajati;
        }

        public static int VerifyTypeOfUser(string username)
        {
            foreach (Utilizator user in listaAngajati)
                if (user.email == username)
                    return 1;
            foreach (Utilizator user in listaClienti)
                if (user.email == username)
                    return 2;
            return 0;
        }

        public static Utilizator FindUser(string username)
        {
            foreach (Utilizator user in listaAngajati)
                if (user.email == username)
                    return user;
            foreach (Utilizator user in listaClienti)
                if (user.email == username)
                    return user;
            return null;
        }

        public static bool VerifyPassword(Utilizator user,string password)
        {
            if (user.parola == password)
            {
                currentUser = user;
                return true;
            }
            return false;
        }

        public static int GetNumberOfUsers()
        {
            return listaAngajati.Count + listaClienti.Count; 
        }

    }
}
