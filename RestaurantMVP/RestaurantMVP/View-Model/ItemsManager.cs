﻿using RestaurantMVP.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantMVP.View_Model
{
    class ItemsManager
    {
        public static RestaurantDataContext dc = new RestaurantDataContext(Properties.Settings.Default.RestaurantConnectionString);
        public static List<Preparat> listaPreparate = new List<Preparat>();
        public static List<Meniu> listaMeniuri = new List<Meniu>();

        public static void PopulateListOfPreparate()
        {
            IEnumerable<Preparat> preparate = from p in dc.Preparats select p;
            foreach (Preparat preparat in preparate)
                listaPreparate.Add(preparat);
        }

        public static void PopulateListOfMeniuri()
        {
            IEnumerable<Meniu> meniuri = from m in dc.Menius select m;
            foreach (Meniu meniu in meniuri)
                listaMeniuri.Add(meniu);
        }
    }
}
